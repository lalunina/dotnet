﻿using System;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Permissions;

namespace WinFormsThreadSample
{
    //Написать приложение, показывающее диалоговое окно с полем ввода для указания каталога и кнопкой для показа
    //немодального диалогового окна.В этом окне показывается постоянно обновляющаяся информация о количестве файлов
    //в этом каталоги и их суммарном размере.
    //
    public class NMForm : Form
    {
        private Form form = new Form();

        public TextBox textBox = new TextBox();
        DirectoryInfo dirInfo;
        public NMForm()
        {
            textBox.BorderStyle = BorderStyle.FixedSingle;
            textBox.Multiline = true;
            textBox.ScrollBars = ScrollBars.Vertical;
            textBox.Size = new Size(300, 400);


            form.Size = new Size(400, 400);
            form.Controls.Add(textBox);
          
        }
        public void Start()
        {
            form.ShowDialog();
        }
        public void SetText(string i)
        {
            //   if(Exists(i)) //http://csharpcoderr.com/2012/07/folder-size.html
            // файлосистемный дозор https://docs.microsoft.com/en-us/dotnet/api/system.io.filesystemwatcher?view=netframework-4.8

            textBox.Text = i;

        }

        
    }
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        public NMForm nMForm = new NMForm();
        public DirectoryInfo dir;


        private void buttonThread1_Click(object sender, EventArgs e)
        {
            //
            dir = new DirectoryInfo(textboxhread1.Text);
          Thread  t = new Thread(new ThreadStart(ThreadProc2));
            t.IsBackground = true;
            t.Start();
            nMForm.Start();

        }



        private void labelThread1_Click(object sender, EventArgs e)
        {
            textboxhread1.Text = "";
        }




        //
        // Поток 2
        //
        private void ThreadProc2()
        {

            if (Directory.Exists(textboxhread1.Text))
            {
                FileInfo[] files = dir.GetFiles();

                // Сколько файлов найдено
                nMForm.textBox.Text += "Файлов: " + files.Length.ToString() + Environment.NewLine;
                Thread.Sleep(500);
                string[] dirs = Directory.GetFiles(textboxhread1.Text);
                int x = 0;
                foreach (string s in dirs)
                {
                    nMForm.textBox.Text += s.Length.ToString() +Environment.NewLine;
                    //  nMForm.textBox.Text += "";
                    x += s.Length;

                    Thread.Sleep(500);
                }
            
            nMForm.textBox.Text += "Общий объем: " + x.ToString() + Environment.NewLine;
                Thread.Sleep(500);
            }

            FileSystemWatcher fsw = new FileSystemWatcher(textboxhread1.Text);

                  //  Watch for changes in LastAccess and LastWrite times, and
                  //  the renaming of files or directories.
                  fsw.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite
                      | NotifyFilters.FileName | NotifyFilters.DirectoryName;

                  //  Register a handler that gets called when a
                  //  file is created, changed, or deleted.
                  fsw.Changed += new FileSystemEventHandler(wathing);

                  fsw.Created += new FileSystemEventHandler(wathing);

                  fsw.Deleted += new FileSystemEventHandler(wathing);


                  //  Register a handler that gets called if the
                  //  FileSystemWatcher needs to report an error.
                  fsw.Error += new ErrorEventHandler(OnError);

                  //  Begin watching.
                  fsw.EnableRaisingEvents = true;

                  Console.WriteLine("Press \'Enter\' to quit the sample.");
                  Console.ReadLine();
            
        }

        private void wathing (object source, FileSystemEventArgs e)
        {
            if (Directory.Exists(textboxhread1.Text))
            {
                FileInfo[] files = dir.GetFiles();

                // Сколько файлов найдено
                nMForm.textBox.Text += "Произошли изменения!!!" + Environment.NewLine; 
                Thread.Sleep(500);
                nMForm.textBox.Text += "Файлов: "+ files.Length.ToString() + Environment.NewLine;
                Thread.Sleep(500);
                string[] dirs = Directory.GetFiles(textboxhread1.Text);
                int x = 0;
                foreach (string s in dirs)
                {
                    nMForm.textBox.Text += s.Length.ToString() + Environment.NewLine;
                    //  nMForm.textBox.Text += "";
                    x += s.Length;

                    Thread.Sleep(500);
                }

                nMForm.textBox.Text += "  Общий объем: " + x.ToString() + Environment.NewLine;
                Thread.Sleep(500);
            }
        }

        private void OnError(object source, ErrorEventArgs e)
            {
            
            //  Show that an error has been detected.
            Console.WriteLine("The FileSystemWatcher has detected an error");
                //  Give more information if the error is due to an internal buffer overflow.
                if (e.GetException().GetType() == typeof(InternalBufferOverflowException))
                {
                    //  This can happen if Windows is reporting many file system events quickly
                    //  and internal buffer of the  FileSystemWatcher is not large enough to handle this
                    //  rate of events. The InternalBufferOverflowException error informs the application
                    //  that some of the file system events are being lost.
                    Console.WriteLine(("The file system watcher experienced an internal buffer overflow: " + e.GetException().Message));
                }

            } 

     }
        
}
