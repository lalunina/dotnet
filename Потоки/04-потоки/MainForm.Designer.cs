﻿using System.Drawing;

namespace WinFormsThreadSample
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose (bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textboxhread1 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // buttonThread1
            // 
            this.textboxhread1.Location = new System.Drawing.Point(20, 60);
            this.textboxhread1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.textboxhread1.Name = "buttonThread1";
            this.textboxhread1.Size = new System.Drawing.Size(330, 40);
            this.textboxhread1.BackColor = Color.White;
            this.textboxhread1.TabIndex = 0;
            this.textboxhread1.Text = "Укажите католог";
           // this.textboxhread1.Text = "\"C:\Downloads\install\"";
          //  this.labelThread1.UseVisualStyleBackColor = true;

            this.textboxhread1.Click += new System.EventHandler(this.labelThread1_Click);
            // 


            this.booton1Thread1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // booton1Thread1
            // 
            this.booton1Thread1.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.booton1Thread1.Location = new System.Drawing.Point(111, 184);
            this.booton1Thread1.Name = "booton1Thread1";
            this.booton1Thread1.Size = new System.Drawing.Size(75, 23);
            this.booton1Thread1.TabIndex = 1;
            this.booton1Thread1.Text = "OK";
            this.booton1Thread1.UseVisualStyleBackColor = false;
            this.booton1Thread1.Click += new System.EventHandler(this.buttonThread1_Click);
            // 
            // NMForm
            // 
            this.ClientSize = new System.Drawing.Size(282, 253);
            this.Controls.Add(this.booton1Thread1);
            this.Name = "NMForm";
            this.ResumeLayout(false);

            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(376, 214);
            this.Controls.Add(this.textboxhread1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "WinForms Thread Sample";
            
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox textboxhread1;
        private System.Windows.Forms.Button booton1Thread1;
    }
}

