﻿using System;

namespace WinFormsThreadSample
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Start = new System.Windows.Forms.Button();
            this.Throw = new System.Windows.Forms.Button();
            this.labelThread1 = new System.Windows.Forms.Label();
            this.labelThread2 = new System.Windows.Forms.Label();
            this.Stop = new System.Windows.Forms.Button();
            this.labelThread3 = new System.Windows.Forms.Label();
            this.labelStatus1 = new System.Windows.Forms.Label();
            this.labelStatus2 = new System.Windows.Forms.Label();
            this.labelStatus3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Start
            // 
            this.Start.Location = new System.Drawing.Point(32, 60);
            this.Start.Name = "Start";
            this.Start.Size = new System.Drawing.Size(90, 41);
            this.Start.TabIndex = 0;
            this.Start.Text = "Start";
            this.Start.UseVisualStyleBackColor = true;
            this.Start.Click += new System.EventHandler(this.Start_Click);
            // 
            // Throw
            // 
            this.Throw.Location = new System.Drawing.Point(157, 60);
            this.Throw.Name = "Throw";
            this.Throw.Size = new System.Drawing.Size(107, 41);
            this.Throw.TabIndex = 0;
            this.Throw.Text = "Throw";
            this.Throw.UseVisualStyleBackColor = true;
            this.Throw.Click += new System.EventHandler(this.Throw_Click);
            // 
            // labelThread1
            // 
            this.labelThread1.Location = new System.Drawing.Point(53, 138);
            this.labelThread1.Name = "labelThread1";
            this.labelThread1.Size = new System.Drawing.Size(45, 23);
            this.labelThread1.TabIndex = 1;
            this.labelThread1.Text = "0";
            this.labelThread1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelThread2
            // 
            this.labelThread2.Location = new System.Drawing.Point(176, 138);
            this.labelThread2.Name = "labelThread2";
            this.labelThread2.Size = new System.Drawing.Size(45, 23);
            this.labelThread2.TabIndex = 1;
            this.labelThread2.Text = "0";
            this.labelThread2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Stop
            // 
            this.Stop.Location = new System.Drawing.Point(295, 60);
            this.Stop.Name = "Stop";
            this.Stop.Size = new System.Drawing.Size(107, 41);
            this.Stop.TabIndex = 2;
            this.Stop.Text = "Stop";
            this.Stop.UseVisualStyleBackColor = true;
            this.Stop.Click += new System.EventHandler(this.Stop_Click);
            // 
            // labelThread3
            // 
            this.labelThread3.Location = new System.Drawing.Point(329, 138);
            this.labelThread3.Name = "labelThread3";
            this.labelThread3.Size = new System.Drawing.Size(45, 23);
            this.labelThread3.TabIndex = 3;
            this.labelThread3.Text = "0";
            this.labelThread3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelStatus
            // 
            this.labelStatus1.AutoSize = true;
            this.labelStatus1.Location = new System.Drawing.Point(46, 200);
            this.labelStatus1.Name = "labelStatus";
            this.labelStatus1.Size = new System.Drawing.Size(48, 17);
            this.labelStatus1.TabIndex = 4;
            this.labelStatus1.Text = "Status";
            // 
            // label1
            // 
            this.labelStatus2.AutoSize = true;
            this.labelStatus2.Location = new System.Drawing.Point(176, 200);
            this.labelStatus2.Name = "label1";
            this.labelStatus2.Size = new System.Drawing.Size(48, 17);
            this.labelStatus2.TabIndex = 5;
            this.labelStatus2.Text = "Status";
            // 
            // label2
            // 
            this.labelStatus3.AutoSize = true;
            this.labelStatus3.Location = new System.Drawing.Point(326, 200);
            this.labelStatus3.Name = "label2";
            this.labelStatus3.Size = new System.Drawing.Size(48, 17);
            this.labelStatus3.TabIndex = 6;
            this.labelStatus3.Text = "Status";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(433, 274);
            this.Controls.Add(this.labelStatus3);
            this.Controls.Add(this.labelStatus2);
            this.Controls.Add(this.labelStatus1);
            this.Controls.Add(this.labelThread3);
            this.Controls.Add(this.Stop);
            this.Controls.Add(this.labelThread2);
            this.Controls.Add(this.labelThread1);
            this.Controls.Add(this.Throw);
            this.Controls.Add(this.Start);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "WinForms Thread Sample";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

      

        #endregion

        private System.Windows.Forms.Button Start;
        private System.Windows.Forms.Button Throw;
        private System.Windows.Forms.Label labelThread1;
        private System.Windows.Forms.Label labelThread2;
        private System.Windows.Forms.Button Stop;
        private System.Windows.Forms.Label labelThread3;
        private System.Windows.Forms.Label labelStatus1;
        private System.Windows.Forms.Label labelStatus2;
        private System.Windows.Forms.Label labelStatus3;
    }
}

