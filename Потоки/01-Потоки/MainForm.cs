﻿
using System;
using System.Drawing.Text;
using System.Threading;
using System.Windows.Forms;

namespace WinFormsThreadSample
{




    public partial class MainForm : Form
    {
        public MainForm()// pfgecrftv ajhve
        {
            InitializeComponent();
        }

        //создаем потоки 
        
        private void ThreadProc1( )
        {
            int i;
            for ( i= 1; i <= 10; i++)
            {
                // В принципе, такое обращение должно упасть с InvalidOperationException
                labelThread1.Text = i.ToString();
                Thread.Sleep(500);
            }
        }
        private void ThreadProc2()
        {
            for (int i = 1; i <= 10; i++)
            {
                // В принципе, такое обращение должно упасть с InvalidOperationException
                labelThread2.Text = i.ToString();
                Thread.Sleep(1000);
            }
        }

            private  void ThreadProc3()
        {
            for (int i = 1; i <= 10; i++)
            {
                // В принципе, такое обращение должно упасть с InvalidOperationException
                labelThread3.Text = i.ToString();
                Thread.Sleep(1500);
            }
        }

       public Thread t1  = new Thread(new ThreadStart(Count));

        private static void Count()
        {
            throw new NotImplementedException();
        }

       
        public Thread t2 = new Thread(new ThreadStart(Count));
        public  Thread t3 = new Thread(new ThreadStart(Count));


        private void Start_Click(object sender, EventArgs e)
        {
           

            if (t1.ThreadState == ThreadState.Unstarted)
            {
                t1 = new Thread(new ThreadStart(ThreadProc1));
                t2 = new Thread(new ThreadStart(ThreadProc2));
                t3 = new Thread(new ThreadStart(ThreadProc3));
               
                t1.IsBackground = true;
                t1.Start();
                //
                t2.IsBackground = true;
                t2.Start();
                //
                t3.IsBackground = true;
                t3.Start();

                labelStatus1.Text= t1.IsAlive.ToString();
                labelStatus2.Text = t1.IsAlive.ToString();
                labelStatus3.Text = t1.IsAlive.ToString();
                
                //labelStatus1.Text = t1.ThreadState.ToString();
                //labelStatus2.Text = t2.ThreadState.ToString();
                //labelStatus3.Text = t3.ThreadState.ToString();

            }
           // else 
           //{
           //    t1.Interrupt();
           //    t2.Interrupt();
           //    t3.Interrupt();
           //}
           //
        }


        private void Throw_Click(object sender, EventArgs e)
        {
            
            Thread.Sleep(500);

            labelStatus1.Text = t1.ThreadState.ToString();
            labelStatus2.Text = t2.ThreadState.ToString();
            labelStatus3.Text = t3.ThreadState.ToString();
            Thread.Sleep(5000);
            //Thread.Sleep(Timeout.Infinite);


            // t2.Interrupt(20);
            // t3.Interrupt();

        }



        private void Stop_Click(object sender, EventArgs e)
        {
            
            t1.Abort();
            t2.Abort();
            t3.Abort();
            labelStatus1.Text = t1.ThreadState.ToString();
            labelStatus2.Text = t2.ThreadState.ToString();
            labelStatus3.Text = t3.ThreadState.ToString();
            Thread.Sleep(1000);
            t1 = new Thread(new ThreadStart(ThreadProc1));
            t2 = new Thread(new ThreadStart(ThreadProc2));
            t3 = new Thread(new ThreadStart(ThreadProc3));

           // labelStatus1.Text = t1.ThreadState.ToString();
            //labelStatus2.Text = t2.ThreadState.ToString();
            //labelStatus3.Text = t3.ThreadState.ToString();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

       
    }
}
