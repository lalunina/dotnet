﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace WinFormsThreadSample
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void ThreadProc1()
        {
            for (int i = 1; i <= Convert.ToInt32(textBox1.Text); i++)
            {
                int y = i;
                labelThread1.BeginInvoke((MethodInvoker)(() => labelThread1.Text = (Convert.ToInt32(labelThread1.Text)+y).ToString()));
               
                Thread.Sleep(500);
            }
        }

        private void buttonThread1_Click(object sender, EventArgs e)
        {
            var t = new Thread(new ThreadStart(ThreadProc1));
            //t.IsBackground = true;
            t.Start();
        }

        private void ThreadProc2()
        {
            for (int i = 1; i <= Convert.ToInt32(textBox2.Text); i++)
            {
                int y = i;
                labelThread2.BeginInvoke((MethodInvoker)(() => labelThread2.Text = (Convert.ToInt32(labelThread2.Text) + y).ToString()));
               
                Thread.Sleep(500);
                
            }
        }

        private void buttonThread2_Click(object sender, EventArgs e)
        {
            var t = new Thread(new ThreadStart(ThreadProc2));
            //t.IsBackground = true;
            t.Start();
        }

       
    }
}
