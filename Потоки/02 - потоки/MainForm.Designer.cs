﻿namespace WinFormsThreadSample
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonThread1 = new System.Windows.Forms.Button();
            this.buttonThread2 = new System.Windows.Forms.Button();
            this.labelThread1 = new System.Windows.Forms.Label();
            this.labelThread2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // buttonThread1
            // 
            this.buttonThread1.Location = new System.Drawing.Point(68, 60);
            this.buttonThread1.Name = "buttonThread1";
            this.buttonThread1.Size = new System.Drawing.Size(107, 41);
            this.buttonThread1.TabIndex = 0;
            this.buttonThread1.Text = "Thread #1";
            this.buttonThread1.UseVisualStyleBackColor = true;
            this.buttonThread1.Click += new System.EventHandler(this.buttonThread1_Click);
            // 
            // buttonThread2
            // 
            this.buttonThread2.Location = new System.Drawing.Point(208, 60);
            this.buttonThread2.Name = "buttonThread2";
            this.buttonThread2.Size = new System.Drawing.Size(107, 41);
            this.buttonThread2.TabIndex = 0;
            this.buttonThread2.Text = "Thread #2";
            this.buttonThread2.UseVisualStyleBackColor = true;
            this.buttonThread2.Click += new System.EventHandler(this.buttonThread2_Click);
            // 
            // labelThread1
            // 
            this.labelThread1.Location = new System.Drawing.Point(94, 138);
            this.labelThread1.Name = "labelThread1";
            this.labelThread1.Size = new System.Drawing.Size(45, 23);
            this.labelThread1.TabIndex = 1;
            this.labelThread1.Text = "0";
            this.labelThread1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelThread2
            // 
            this.labelThread2.Location = new System.Drawing.Point(232, 138);
            this.labelThread2.Name = "labelThread2";
            this.labelThread2.Size = new System.Drawing.Size(45, 23);
            this.labelThread2.TabIndex = 1;
            this.labelThread2.Text = "0";
            this.labelThread2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(68, 12);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 22);
            this.textBox1.TabIndex = 2;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(215, 12);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 22);
            this.textBox2.TabIndex = 3;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(376, 214);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.labelThread2);
            this.Controls.Add(this.labelThread1);
            this.Controls.Add(this.buttonThread2);
            this.Controls.Add(this.buttonThread1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "WinForms Thread Sample";
            
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonThread1;
        private System.Windows.Forms.Button buttonThread2;
        private System.Windows.Forms.Label labelThread1;
        private System.Windows.Forms.Label labelThread2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
    }
}

