﻿namespace WinFormsThreadSample
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonThread1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonThread1
            // 
            this.buttonThread1.Location = new System.Drawing.Point(68, 60);
            this.buttonThread1.Name = "buttonThread1";
            this.buttonThread1.Size = new System.Drawing.Size(160, 41);
            this.buttonThread1.TabIndex = 0;
            this.buttonThread1.Text = "Thread #1";
            this.buttonThread1.UseVisualStyleBackColor = true;
            this.buttonThread1.Click += new System.EventHandler(this.buttonThread1_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(376, 214);
            this.Controls.Add(this.buttonThread1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "WinForms Thread Sample";
            
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonThread1;
    }
}

