﻿using System;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormsThreadSample
{

    public class NMForm : Form
    {
        private Form form = new Form();

        private Label label = new Label();
        public NMForm()
        {
            label.BorderStyle = BorderStyle.FixedSingle;

            label.Width = 30;
            label.Height = 30;
            //label.Text = "5";
            form.Size = new Size(200, 100);
            form.Controls.Add(label);   
        }
        public void Start()
        {
            form.ShowDialog();
        }
       public  void SetText(int i)
        {
            label.Text = i.ToString();
        }

    }
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }
    
        public NMForm nMForm = new NMForm();

        private void buttonThread1_Click(object sender, EventArgs e)
        {
            //
            var t = new Thread(new ThreadStart(ThreadProc2));
            t.IsBackground = true;
            t.Start();
            nMForm.Start();
        }

        //
        // Поток 2
        //
        private void ThreadProc2()
        {
            
            for (int i = 1; i <= 10; i++)
            {
               // nMForm.label.Text=i.ToString();
                nMForm.SetText(i);
                Thread.Sleep(500);
            }
            Thread.ResetAbort();

        } 
     

        
    }
    

}
